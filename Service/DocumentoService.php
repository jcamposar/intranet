<?php
include ("../Models/Documento.php");

class DocumentoService{
    public function DocumentAll(){
        $con=new Conection();
        $cont=0;
        $json=[];//array de documentos
        $i=0;
        $doc=$con->selectall("Select * from documento");
        foreach ($doc as $value){
            $documento= new Documento();
            $documento->id = $value["id"];
            $documento->nombre= $value["nombre"];
            $documento->tipoDoc= $value["idTipoDoc"];
            $documento->receptor= $value["receptor"];
            $documento->fecha= $value["fecha"];
            $documento->estado= $value["estado"];
            $json[$cont]=$documento;
            $cont++;
        }
        echo json_encode($json);
    }
    public function documentoId($id){
        $con=new Conection();
        $doc=$con->selectall("SELECT * FROM documento WHERE `receptor`='".$id."'");
        $cont=0;
        $json=[];//array de documentos
        $i=0;
        if ($doc!=0){
            foreach ($doc as $value){
                $documento= new Documento();
                $documento->id = $value["id"];
                $documento->nombre= $value["nombre"];
                $documento->tipoDoc= $con->select("SELECT * FROM tipodoc WHERE `id`='".$value["idTipoDoc"]."'"); 
                $documento->idPerfil= $con->select("SELECT * FROM perfil WHERE `id`='".$value["idPerfil"]."'");
                $documento->receptor= $value["receptor"];
                $documento->fecha= $value["fecha"];
                $documento->estado= $value["estado"];
                $json[$cont]=$documento;
                $cont++;
            }
            echo json_encode($json);
        }
    }
    public function SaveDoc($json){
        $con=new Conection();
        $documento= new Documento();
        $documento->nombre=$json->nombre;
        $documento->tipoDoc= $json->idTipoDoc; 
        $documento->idPerfil=$json->idPerfil;
        $documento->receptor= $json->receptor;
        $documento->fecha=$json->fecha;
        $documento->estado= 1;
        $in=$con->insert("INSERT INTO `documento`(`nombre`, `idTipoDoc`, `receptor`, `idPerfil`, `fecha`, `estado`) VALUES ( '".$documento->nombre."','".$documento->tipoDoc."','".$documento->receptor."','".$documento->idPerfil."','".$documento->fecha."','".$documento->estado."')");
        echo json_encode($in);
    }
}
?>