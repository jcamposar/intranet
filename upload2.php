<?php
if (($_FILES["file"]["type"] == "image/pjpeg")
    || ($_FILES["file"]["type"] == "image/jpeg")
    || ($_FILES["file"]["type"] == "image/png")
    || ($_FILES["file"]["type"] == "image/gif")) {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], "img/".$_FILES['file']['name'])) {
        //more code here...
        echo "./img/".$_FILES['file']['name'];
    } else {
        echo 0;
    }
}
else if (($_FILES["file"]["type"] == "application/pdf")
    || ($_FILES["file"]["type"] == "application/docx")
    || ($_FILES["file"]["type"] == "application/doc")
    || ($_FILES["file"]["type"] == "application/xls") 
    || ($_FILES["file"]["type"] == "application/vnd.ms-excel") 
    || ($_FILES["file"]["type"] == "application/xlsx")) {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], "img/doc/".$_FILES['file']['name'])) {
        //more code here...
        echo "./img/doc/".$_FILES['file']['name'];
    } else {
        echo 0;
    }
} else {
    echo 0;
}
?>